var $$ = document.querySelectorAll.bind(document);
var $ = document.querySelector.bind(document);

var elements = $$(".el");
elements.forEach(function (button) {
    button.addEventListener("click", function (e) {
        e.preventDefault();
        val = this.getAttribute('data-value');
        console.log(val);
        var result = $("#result");
        var rValue = result.innerHTML;
        if (!rValue && checkChar(val)) {
            return;
        } else if (parseInt(val) !== NaN) {
            result.innerHTML += val
        }
    });
});


function checkChar(str) {
    if (str) {
        return (
            str.indexOf('.') === 0 ||
            str.indexOf('/') === 0 ||
            str.indexOf('*') === 0 ||
            str.indexOf('-') === 0 ||
            str.indexOf('+') === 0)
    }
}

function checkCharAtLast(str) {
    var lastChar = str[str.length - 1];
    return (
        lastChar == '.' ||
        lastChar == '/' ||
        lastChar == '*' ||
        lastChar == '-' ||
        lastChar == '+')
}

//  result calculator
$("#solve").addEventListener('click', function (e) {
    e.preventDefault();
    let x = $("#result").innerHTML;
    if (checkCharAtLast(x)) {
        x = x.slice(0, -1);
    }
    if (x) {
        let y = eval(x)
        document.getElementById("result").innerHTML = y
    }

});

$("#clear").addEventListener("click", function () {
    $("#result").innerHTML = ""
});